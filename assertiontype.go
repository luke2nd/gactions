package gactions

// AssertionType is type that the app can support at the token endpoint.
type AssertionType int

const (
	// UnknownAssertionType for backwards compatability. Rejected.
	UnknownAssertionType AssertionType = iota

	// TokenID is OpenIDConnect ID token. This is JWT token with grantType set to urn:ietf:params:oauth:grant-type:jwt-bearer. This is the same token as produced by Google Sign-In libraries, and its verification is documented at https://developers.google.com/identity/sign-in/web/backend-auth. For more information on JWT tokens, see https://tools.ietf.org/html/rfc7523#section-2.1
	// This token will include the non-standard key-value pair intent=get when the user attempts to link to an existing account.
	// The response should be as per https://tools.ietf.org/html/rfc6749#section-4.1.4 in the event of success, and as per https://tools.ietf.org/html/rfc6749#section-4.2.2.1 in the event of error.
	TokenID

	// AccountCreation is the same as ID_TOKEN, except that a non-standard key "intent" will be set to "create" indicating that the user is atempting to create a new account that should be associated with the Google ID in the sub field of the JWT, as with other usage of Google Sign-In. The responses are as with ID_TOKEN.
	AccountCreation
)
