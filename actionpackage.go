package gactions

// ActionPackage holds the content for the draft of an App as well as each deployed version. This includes directory listing details, conversation configuration and account linking.
type ActionPackage struct {
	// The details of the App. This is where the directory listing is kept as well as other App identification like displayName.
	Manifest Manifest `json:"manifest,omitempty"`
	// The details for account linking on this App.
	AccountLinking AccountLinking `json:"accountLinking,omitempty"`
	// List of actions the App is able to handle.
	Actions []Action `json:"actions,omitempty"`
	// List of types defined by the developer.
	Types []Type `json:"types,omitempty"`
	// Map conversations that can be shared across Actions. For example see the conversationName in Fulfillment.
	// An object containing a list of "key": value pairs. Example: { "name": "wrench", "mass": "1.3kg", "count": "3" }.
	Conversations map[string]ConversationFulfillment `json:"conversations,omitempty"`
}

// Manifest is the set of metadata for the Agent. The contents of this message are used in multiple contexts:
//  Actions on Google directory listing page
//  Unique identification of an Agent for explicit invocation (displayName)
//  Content for other parts of an ActionPackage (shortDescription is used for display in AccountLinking, displayName is used in Actions to show the Agent name to users).
//  This message is read-only. You specify the fields in this message via the Actions console and not directly in the action package.
type Manifest struct {
	// The default display name for this Agent (if there is not a translation available), e.g. "Starbucks". This is also used as a method for users to invoke this Agent (in addition to invocationName). The display name must be unique and phonetically very similar to invocationName.
	DisplayName string `json:"displayName,omitempty"`
	// This is the unique name for this Agent to directly invoke it within a voice (spoken) context. Policies require that the invocation name is phonetically very similar to the displayName field.
	InvocationName string `json:"invocationName,omitempty"`
	// The default short description for the Agent (if there is not a translation available). This is limited to 80 characters (consistent with Play store).
	ShortDescription string `json:"shortDescription,omitempty"`
	// The default long description for the Agent (if there is not a translation available). This is limited to 4000 characters (consistent with Play store).
	LongDescription string `json:"longDescription,omitempty"`
	// The category for the Agent. The value must be one of the allowed categories for Agents. See the Actions on Google console for the list of allowed categories.
	Category string `json:"category,omitempty"`
	// Small square image. The dimensions must be 192px by 192px.
	SmallSquareLogoURL string `json:"smallSquareLogoUrl,omitempty"`
	// Large landscape image. The dimensions musst be 2208px by 1242px.
	LargeLandscapeLogoURL string `json:"largeLandscapeLogoUrl,omitempty"`
	// The name of the company that the Agent is associated with.
	CompanyName string `json:"companyName,omitempty"`
	// The contact email address to allow users to reach out regarding the Agent.
	ContactEmail string `json:"contactEmail,omitempty"`
	// The URL to the ToS (Terms of Service) for the Agent.
	TermsOfServiceURL string `json:"termsOfServiceUrl,omitempty"`
	// The URL for the Agent's privacy policy.
	PrivacyURL string `json:"privacyUrl,omitempty"`
	// Sample invocation phrase displayed as part of Agent description in the directory of all Agents. Only 5 values can be given.
	SampleInvocation []string `json:"sampleInvocation,omitempty"`
	// Summary of what an Agent can do. Used for Agent introduction to the users. This should be a verb phrase that completes a sentence like "You can use this Agent to..."
	Introduction string `json:"introduction,omitempty"`
	// Free-form testing instructions for the Agent reviewer.
	TestingInstructions string `json:"testingInstructions,omitempty"`
	// The voice name to be used. Example values supported: - male_1 - male_2 - female_1 - female_2
	VoiceName string `json:"voiceName,omitempty"`
	// A set of requirements for the Google Assistant client surface that must be met for the Agent to be triggered.
	SurfaceRequirements SurfaceRequirements `json:"surfaceRequirements,omitempty"`
}

// SurfaceRequirements contains a set of requirements for the client surface that must be met for the Agent to be triggered. If any requirement listed here is not met, the agent will not be triggered.
type SurfaceRequirements struct {
	//The minimum set of capabilities needed for the agent to function. If the surface is missing any of these, the agent will not be triggered.
	MinimumCapabilities []Capability `json:"minimumCapabilities,omitempty"`
}

// Capability represents a requirement about the availability of a given capability
type Capability struct {
	// The name of the capability, e.g. actions.capability.AUDIO_OUTPUT
	Name string `json:"name,omitempty"`
}

// Type that can be referenced within an Action. These can be custom types unique to the action or common types defined by Google and referenced within the action.
type Type struct {
	// Name of the custom type, in the format of Type.
	Name string `json:"name,omitempty"`
	// List of entities for this type. Each includes a key and list of synonyms.
	Entities []Entity `json:"entities,omitempty"`
	// Whether the entities are user-defined (different for every user).
	IsUserDefined bool `json:"isUserDefined,omitempty"`
}

// Entity corresponds is unique for this type. The key should be unique and the list of synonyms are used for triggering.
type Entity struct {
	Key      string   `json:"key,omitempty"`      // Unique key for this item.
	Synonyms []string `json:"synonyms,omitempty"` // List of synonyms which can be used to refer to this item.
}
