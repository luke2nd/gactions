package gactions

// Action represents a specific interaction App supports, it can be a single shot action, or a multi-step interaction.
type Action struct {
	// Name of the action. If a built-in action name is used (starts with actions.),
	// intent and description are implied. They must be blank.
	// For built-in intents or custom intents, use a custom action name.
	Name string `json:"name,omitempty"`
	// How to execute this action.
	Fulfillment Fulfillment `json:"fulfillment,omitempty"`
	// Intent that this action fulfills. Built-in intents start with actions.
	Intent Intent `json:"intent,omitempty"`
	// English description what the action does. This is mainly used for Google to review the action or debugging purpose.
	// This description will not be shown to users. It must be less than 100 ASCII letters.
	Description string `json:"description,omitempty"`
	// Indicates whether sign-in is required for this action. Note this is valid
	// only when App has AccountLinking specified inside Manifest.
	SignInRequired bool `json:"signInRequired,omitempty"`
}

// Fulfillment describes how to execute an action.
type Fulfillment struct {
	// Execute an action via a conversation fulfillment.
	// This references the "name" field of Conversations in ActionPackage.conversations field.
	ConversationName string `json:"conversationName,omitempty"`
}

// Intent and its associated query patterns which are used to match user's queries. Used for initial triggering.
type Intent struct {
	// Indicates the name of this intent, e.g., BOOK_A_TABLE. Can be a built-in intent, starting with actions.,
	// in which case parameters and query patterns are implied and are therefore ignored.
	Name string `json:"name,omitempty"`
	// The list of parameters within the queryPatterns. All of the parameters within queryPatterns must be given.
	Parameters []Parameter `json:"parameters,omitempty"`
	//Triggering specification for this intent.
	Trigger Trigger `json:"trigger,omitempty"`
}

// Parameter used within query pattterns. This is a structured representation of the parameters in the queryPatterns (e.g. $Color:color).
type Parameter struct {
	// Name of the parameter, e.g. color.
	Name string `json:"name,omitempty"`
	// Type of the parameter. Can be a common type or custom type declared as part of the action package, e.g. Color.
	Type string `json:"type,omitempty"`
}

// Trigger describers a trigger
type Trigger struct {
	// List of patterns used to identify the specified intent.
	// Query patterns must only refer to parameters declared in the parameters field.
	QueryPatterns []string `json:"queryPatterns,omitempty"`
}
