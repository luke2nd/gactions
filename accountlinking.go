package gactions

// AccountLinking is used for the App to allow Google to guide user to sign-in to the App's web services.
type AccountLinking struct {
	// Unique public string used to identify client requesting authentication.
	ClientID string `json:"clientId,omitempty"`
	// Client secret maintained by the developer.
	ClientSecret string `json:"clientSecret,omitempty"`
	// Indicates type of authentication.
	GrantType AuthGrantType `json:"grantType,omitempty"`
	// The url where users will be redirected to for entering login credentials,
	// for example, https://login.example.org/oauth/v2/authorize.
	AuthenticationURL string `json:"authenticationUrl,omitempty"`
	// The url to fetch the access token given an authorization code,
	// for example, https://login.example.org/oauth/v2/token.
	AccessTokenURL string `json:"accessTokenUrl,omitempty"`
	// List of scopes user needs to grant permission for. Up to 10 scopes are supported.
	Scopes []string `json:"scopes,omitempty"`
	// The App's URL for displaying information about the scopes for which
	// the access token is being granted. The URL will be appended with a query parameter
	// "scopes" containing a list of scopes being requested, e.g.: ?scopes=scope1+scope2+scope3.
	ScopeExplanationURL string `json:"scopeExplanationUrl,omitempty"`
	// The Google API Console OAuth 2.0 client ID that is being used
	// by the App for Google Sign-In. This field is required for the ID_TOKEN assertion type,
	// it's used in the aud (audience) field of the ID token: http://openid.net/specs/openid-connect-core-1_0.html#IDToken.
	// See https://developers.google.com/identity/protocols/OpenIDConnect for more information on ID token.
	GoogleSignInClientID string `json:"googleSignInClientId,omitempty"`
	//The list of assertion types that the App can support at the token endpoint.
	AssertionTypes []AssertionType `json:"assertionTypes,omitempty"`
}
