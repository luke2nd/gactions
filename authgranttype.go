package gactions

// AuthGrantType is the grant type Google guides user to sign in to the App's web service.
type AuthGrantType int

const (
	// AuthGrantTypeUnspecified is invalid auth grant type.
	AuthGrantTypeUnspecified AuthGrantType = iota

	// AuthCode grant, requires developer to provide both authentication URL and access token URL.
	AuthCode

	// Implicity code grant, only requires developer to provide authentication URL.
	Implicity
)
