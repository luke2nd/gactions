package gactions

// CustomType allows you to define your own types, which are a collection of items, the possible ways that user might say an item (synonyms), and the corresponding argument value (key).
type CustomType struct {
	Name  string `json:"name,omitempty"` // Name, e.g. $MorningOptions
	Items Item   `json:"items,omitempty"`
}

// Item defines CustomType item
type Item struct {
	Key      string   `json:"key,omitempty"`      // The following example defines a custom type for the different ways a person might say "6am". e.g. 6am
	Synonyms []string `json:"synonyms,omitempty"` // e.g. "6 am", "6 o clock", "oh six hundred", "6 in the morning"
}
