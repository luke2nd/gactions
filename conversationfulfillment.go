package gactions

// ConversationFulfillment defines conversation fulfillment implementation.
type ConversationFulfillment struct {
	// The unique name for this conversation.
	Name string `json:"name,omitempty"`
	// The HTTPS endpoint for the conversation (HTTP is not supported).
	// This can be a Dialogflow endpoint, a Google Cloud Function endpoint or a self-hosting HTTPS endpoint.
	// Google sends a POST request to exchange data with 3P.
	URL string `json:"url,omitempty"`
	// Map of HTTP parameters to be included in the POST request.
	// map (key: string, value: string)
	// An object containing a list of "key": value pairs. Example: { "name": "wrench", "mass": "1.3kg", "count": "3" }.
	HTTPHeaders map[string]string `json:"httpHeaders,omitempty"`
	// API version used when communicating with App.
	FulfillmentAPIVersion uint `json:"fulfillmentApiVersion,omitempty"`
}
