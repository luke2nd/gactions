package webhook

import "time"

const (
	DefaultConversationToken = "{\"state\":null,\"data\":{}}"
)

// AppRequest is sent by the Google Assistant to apps to ask for the app to drive feature-specific conversations.
// The API version is specified in the HTTP header. For API version 1, the header contains:
// Google-Assistant-API-Version: v1. For API version 2, the header contains: Google-Actions-API-Version: 2.
type AppRequest struct {
	// User who initiated the conversation.
	User User `json:"user,omitempty"`
	// Information about the device the user is using to interact with the app.
	Device Device `json:"device,omitempty"`
	// Holds session data like the conversation ID and conversation token.
	Conversation Conversation `json:"conversation,omitempty"`
	// List of inputs corresponding to the expected inputs specified by the app. For the initial conversation trigger, the input contains information on how the user triggered the conversation.
	Inputs []Input `json:"inputs,omitempty"`
	// Information about the surface the user is interacting with, e.g. whether it can output audio or has a screen.
	Surface Surface `json:"surface,omitempty"`
	// Indicates whether the request should be handled in sandbox mode.
	IsInSandbox bool `json:"isInSandbox,omitempty"`
}

// Permission idetifies user's permission.
type Permission string

const (
	UnspecifiedPermission           Permission = "UNSPECIFIED_PERMISSION"
	NamePermission                  Permission = "NAME"                    // Ask for user's first and last name.
	EmailPermission                 Permission = "EMAIL"                   // Ask for user's email address.
	DevicePreciseLocationPermission Permission = "DEVICE_PRECISE_LOCATION" // Ask for user's precise location, lat/lng and formatted address. Returns only lat/lng on phones.
	DeviceCoarseLocationPermission  Permission = "DEVICE_COARSE_LOCATION"  // Ask for user's coarse location, zip code, city and country code. Works only from Google Home devices.
	PhonePermission                 Permission = "PHONE_NUMBER"            // Ask for user's phone number.
	UpdatePermission                Permission = "UPDATE"                  // Ask for permissions to send updates.
)

// User is the owner of the request/conversation
type User struct {
	// Unique ID for the end user.
	UserID string `json:"userId,omitempty"`
	// Information about the end user. Some fields are only available if the user has given permission to provide this information to the app.
	Profile UserProfile `json:"profile,omitempty"`
	// An OAuth2 token that identifies the user in your system. Only available if [Account Linking][google.actions.v2.AccountLinking] configuration is defined in the action package and the user links their account.
	AccessToken string `json:"accessToken,omitempty"`
	// Contains permissions granted by user to this app.
	Permissions []Permission `json:"permissions,omitempty"`
	// Primary locale setting of the user making the request. Follows IETF BCP-47 language code http://www.rfc-editor.org/rfc/bcp/bcp47.txt However, the script subtag is not included.
	Locale string `json:"locale,omitempty"`
}

// UserProfile contains the user's personal info. Fields are only populated if the user user grants the permission to the app for a particular field.
type UserProfile struct {
	// The user's full name as specified in their Google account. Requires the NAME permission.
	DisplayName string `json:"display_name,omitempty"`
	// The user's first name as specified in their Google account. Requires the NAME permission.
	GivenName string `json:"givenName,omitempty"`
	// The user's last name as specified in their Google account. Note that this field could be empty. Requires the NAME permission.
	FamilyName string `json:"familyName,omitempty"`
	// The user's email address as specified in their Google account. Requires the EMAIL permission.
	Email string `json:"email,omitempty"`
	// The user's phone number as specified in their Google account. Requires the PHONE_NUMBER permission.
	PhoneNumber string `json:"phoneNumber,omitempty"`
}

// Device contains information about the device the user is using to interact with the Google Assistant.
type Device struct {
	Location Location `json:"location,omitempty"`
}

// Location is a container that represents a location.
type Location struct {
	// Geo coordinates. Requires the DEVICE_PRECISE_LOCATION permission.
	Coordinates LatLng `json:"coordinates,omitempty"`
	//Display address, e.g., "1600 Amphitheatre Pkwy, Mountain View, CA 94043". Requires the DEVICE_PRECISE_LOCATION permission.
	FormattedAddress string `json:"formattedAddress,omitempty"`
	// Zip code. Requires the DEVICE_COARSE_LOCATION permission.
	ZipCode string `json:"zipCode,omitempty"`
	// City. Requires the DEVICE_COARSE_LOCATION permission.
	City string `json:"city,omitempty"`
	// Postal address. Requires the DEVICE_COARSE_LOCATION permission
	PostalAddress PostalAddress `json:"postalAddress,omitempty"`
	// Name of the place.
	Name string `json:"name,omitempty"`
	// Phone number of the location, e.g. contact number of business location or phone number for delivery location.
	PhoneNumber string `json:"phoneNumber,omitempty"`
	// Notes about the location.
	Notes string `json:"notes,omitempty"`
}

// LatLng is an object representing a latitude/longitude pair.
// This is expressed as a pair of doubles representing degrees latitude and degrees longitude.
// Unless specified otherwise, this must conform to the WGS84 standard.
// Values must be within normalized ranges.
type LatLng struct {
	Latitude  float32 `json:"latitude,omitempty"`  // The latitude in degrees. It must be in the range [-90.0, +90.0].
	Longitude float32 `json:"longitude,omitempty"` // The longitude in degrees. It must be in the range [-180.0, +180.0].
}

// PostalAddress represents a postal address, e.g. for postal delivery or payments addresses.
// Given a postal address, a postal service can deliver items to a premise, P.O. Box or similar.
// It is not intended to model geographical locations (roads, towns, mountains).
type PostalAddress struct {
	// The schema revision of the PostalAddress. This must be set to 0, which is the latest revision.
	// All new revisions must be backward compatible with old revisions.
	Revision int `json:"revision,omitempty"`
	// Required. CLDR region code of the country/region of the address. This is never inferred and it is up to the user to ensure the value is correct. See http://cldr.unicode.org/ and http://www.unicode.org/cldr/charts/30/supplemental/territory_information.html for details. Example: "CH" for Switzerland.
	RegionCode string `json:"regionCode,omitempty"`
	// Optional. BCP-47 language code of the contents of this address (if known). e.g. "zh-Hant", "ja", "ja-Latn", "en".
	// https://developers.google.com/actions/reference/rest/Shared.Types/Location#LatLng
	LanguageCode string `json:"languageCode,omitempty"`
	// Optional. Postal code of the address. Not all countries use or require postal codes to be present, but where they are used, they may trigger additional validation with other parts of the address (e.g. state/zip validation in the U.S.A.).
	PostalCode string `json:"postalCode,omitempty"`
	// Optional. Additional, country-specific, sorting code. This is not used in most regions. Where it is used, the value is either a string like "CEDEX", optionally followed by a number (e.g. "CEDEX 7"), or just a number alone, representing the "sector code" (Jamaica), "delivery area indicator" (Malawi) or "post office indicator" (e.g. Côte d'Ivoire).
	SortingCode string `json:"sortingCode,omitempty"`
	// Optional. Highest administrative subdivision which is used for postal addresses of a country or region. For example, this can be a state, a province, an oblast, or a prefecture. Specifically, for Spain this is the province and not the autonomous community (e.g. "Barcelona" and not "Catalonia"). Many countries don't use an administrative area in postal addresses. E.g. in Switzerland this should be left unpopulated.
	AdministrativeArea string `json:"administrativeArea,omitempty"`
	// Optional. Generally refers to the city/town portion of the address. Examples: US city, IT comune, UK post town. In regions of the world where localities are not well defined or do not fit into this structure well, leave locality empty and use addressLines.
	Locality string `json:"locality,omitempty"`
	// Optional. Sublocality of the address. For example, this can be neighborhoods, boroughs, districts.
	Sublocality string `json:"sublocality,omitempty"`
	// Unstructured address lines describing the lower levels of an address.
	// https://developers.google.com/actions/reference/rest/Shared.Types/Location#LatLng
	AddressLines []string `json:"addressLines,omitempty"`
	// Optional. The recipient at the address. This field may, under certain circumstances, contain multiline information. For example, it might contain "care of" information.
	Recipients []string `json:"recipients,omitempty"`
	// Optional. The name of the organization at the address.
	Organization string `json:"organization,omitempty"`
}

// ConversationType describes typ of the conversation, there are definced types below
type ConversationType string

const (
	UnspecifiedConversationType ConversationType = "TYPE_UNSPECIFIED" // Unspecified conversation
	NewConversationType         ConversationType = "NEW"              // Newly started conversation.
	ActiveConversationType      ConversationType = "ACTIVE"           // Ongoing conversation.
)

// Conversation contains session info about a conversation
type Conversation struct {
	// Unique ID for the multi-turn conversation. It's assigned for the first turn.
	// After that it remains the same for subsequent conversation turns until the conversation is terminated.
	ConversationID string `json:"conversationId,omitempty"`
	// Type indicates the state of the conversation in its life cycle.
	Type ConversationType `json:"type,omitempty"`
	// Opaque token specified by the app in the last conversation turn.
	// It can be used by an app to track the conversation or to store conversation related data.
	ConversationToken string `json:"conversationToken,omitempty"`
}

type Input struct {
	// Raw input transcription from each turn of conversation that was used to provide this input.
	// Multiple conversation turns that don't involve the app may be required for the assistant to provide some types of input.
	RawInput RawInput `json:"rawInput,omitempty"`
	// Indicates the user's intent. For the first conversation turn, the intent will refer to the intent of the action
	// that is being triggered. For subsequent conversation turns, the intent will be a built-in intent.
	// For example, if the expected input is actions.intent.OPTION, then the the intent specified here will either be
	// actions.intent.OPTION if the Google Assistant was able to satisfy that intent,
	// or actions.intent.TEXT if the user provided other information.
	Intent string `json:"intent,omitempty"`
	// A list of provided argument values for the input requested by the app.
	Arguments []Argument `json:"arguments,omitempty"`
}

// InputType indicates the input source, typed query or voice query.
type InputType string

const (
	UnspecifiedInputType InputType = "UNSPECIFIED_INPUT_TYPE" // Unspecified input source.
	TouchInputType       InputType = "TOUCH"                  // Query from a GUI interaction.
	VoiceInputType       InputType = "VOICE"                  // Voice query.
	KeyboardInputType    InputType = "KEYBOARD"               // Typed query.
)

type RawInput struct {
	CreateTime time.Time `json:"createTime,omitempty"` // When the user provided this input.
	InputType  InputType `json:"inputType,omitempty"`  // Indicates how the user provided this input: a typed response, a voice response, unspecified, etc.
	Query      string    `json:"query,omitempty"`      // Typed or spoken input from the end user.

}

// Surface is information specific to the Google Assistant client surface the user is interacting with.
// Surface is distinguished from Device by the fact that multiple Assistant surfaces may live on the same device.
type Surface struct {
	// A list of capabilities the surface supports at the time of the request e.g. actions.capability.AUDIO_OUTPUT
	Capabilities []Capability `json:"capabilities,omitempty"`
}

// Capability represents a unit of functionality that the surface is capable of supporting.
type Capability struct {
	Name string `json:"name,omitempty"` // The name of the capability, e.g. actions.capability.AUDIO_OUTPUT
}

type AppResponse struct {
	// An opaque token that is recirculated to the app every conversation turn.
	ConversationToken string `json:"conversationToken,omitempty"`
	// Indicates whether the app is expecting a user response.
	// This is true when the conversation is ongoing, false when the conversation is done.
	ExpectUserResponse bool `json:"expectUserResponse,omitempty"`
	// List of inputs the app expects, each input can be a built-in intent, or an input taking list of possible intents.
	// Only one input is supported for now.
	ExpectedInputs []ExpectedInput `json:"expectedInputs,omitempty"`
	// Final response when the app does not expect user's input.
	FinalResponse FinalResponse `json:"finalResponse,omitempty"`
	// Custom Push Message allows developers to send structured data to Google for interactions on the Assistant.
	CustomPushMessage CustomPushMessage `json:"customPushMessage,omitempty"`
	// Metadata passed from bot builder platforms to Google.
	// This metadata can contain error and logging information that bot building platforms want to expose to the app developer.
	ResponseMetadata ResponseMetadata `json:"responseMetadata,omitempty"`
	// Indicates whether the response should be handled in sandbox mode.
	// This bit is needed to push structured data to Google in sandbox mode.
	IsInSandbox bool `json:"isInSandbox,omitempty"`
}

// ResponseMetadata is a container to pass any information from the agent cloud execution to the Assistant.
// User query handler related metadata.
type ResponseMetadata struct {
	Status Status `json:"status,omitempty"` // Status of bot builder 3P invocation.
}

// Status type defines a logical error model that is suitable for different programming environments,
// including REST APIs and RPC APIs. It is used by gRPC. The error model is designed to be:
//   Simple to use and understand for most users
//   Flexible enough to meet unexpected needs
// See https://developers.google.com/actions/reference/rest/Shared.Types/AppResponse#responsemetadata
type Status struct {
	// The status code, which should be an enum value of google.rpc.Code.
	Code int64 `json:"code,omitempty"`
	// A developer-facing error message, which should be in English.
	// Any user-facing error message should be localized and
	// sent in the google.rpc.Status.details field, or localized by the client.
	Message string `json:"message,omitempty"`
	// A list of messages that carry the error details. There is a common set of message types for APIs to use.
	Details []ExtensionType `json:"details,omitempty"`
}

// CustomPushMessage that holds structured data to push for the Actions Fulfillment API.
type CustomPushMessage struct {
	// The specified target for the push request.
	Target Target `json:"target,omitempty"`

	// Union field content can be only one of the following:

	// An order update updating orders placed through transaction APIs.
	OrderUpdate OrderUpdate `json:"orderUpdate,omitempty"`
	// If specified, displays a notification to the user with specified title and text.
	UserNotification UserNotification `json:"userNotification,omitempty"`

	// End of list of possible types for union field content.
}

// Target for the push request.
type Target struct {
	UserID   string   `json:"userId,omitempty"`   // The user to target.
	Intent   string   `json:"intent,omitempty"`   // The intent to target.
	Argument Argument `json:"argument,omitempty"` // The argument to target for an intent. For V1, only one Argument is supported.
}

// Argument to target for an intent
type Argument struct {
	// Name of the argument being provided for the input.
	Name string `json:"name,omitempty"`
	// The raw text, typed or spoken, that provided the value for the argument.
	RawText string `json:"rawText,omitempty"`
	// Specified when query pattern includes a $org.schema.type.Text type or
	// expected input has a built-in intent: actions.intent.TEXT, or actions.intent.OPTION.
	// Note that for the OPTION intent, we set the textValue as option key, the rawText above
	// will indicate the raw span in user's query.
	TextValue string `json:"textValue,omitempty"`

	// Union field value can be only one of the following:

	// Specified when query pattern includes a $org.schema.type.YesNo type or
	// expected input has a built-in intent: actions.intent.CONFIRMATION.
	// NOTE: if the boolean value is missing, it represents false.
	BoolValue bool `json:"boolValue,omitempty"`
	// Specified for the built-in intent: actions.intent.DATETIME.
	DatetimeValue DateTime `json:"datetimeValue,omitempty"`
	// Extension whose type depends on the argument.
	// For example, if the argument name is SIGN_IN for the actions.intent.SIGN_IN intent,
	// then this extension will contain a SignInValue value.
	Extension ExtensionType `json:"extension,omitempty"`
	// End of list of possible types for union field value.
}

// DateTime is date and time argument value parsed from user input. Does not include time zone information.
type DateTime struct {
	Date Date      `json:"date,omitempty"` // Date value
	Time TimeOfDay `json:"time,omitempty"` // Time value
}

// Date represents a whole calendar date, e.g. date of birth.
// The time of day and time zone are either specified elsewhere or are not significant.
// The date is relative to the Proleptic Gregorian Calendar.
// The day may be 0 to represent a year and month where the day is not significant, e.g. credit card expiration date.
// The year may be 0 to represent a month and day independent of year, e.g. anniversary date.
// Related types are google.type.TimeOfDay and google.protobuf.Timestamp.
type Date struct {
	// Year of date. Must be from 1 to 9999, or 0 if specifying a date without a year.
	Year uint16 `json:"year,omitempty"`
	// Month of year. Must be from 1 to 12.
	Month uint8 `json:"month,omitempty"`
	// Day of month. Must be from 1 to 31 and valid for the year and month,
	// or 0 if specifying a year/month where the day is not significant.
	Day uint8 `json:"day,omitempty"`
}

// TimeOfDay represents a time of day.
// The date and time zone are either not significant or are specified elsewhere.
// An API may choose to allow leap seconds.
// Related types are google.type.Date and google.protobuf.Timestamp.
type TimeOfDay struct {
	// Hours of day in 24 hour format. Should be from 0 to 23.
	// An API may choose to allow the value "24:00:00" for scenarios like business closing time.
	Hours uint8 `json:"hours,omitempty"`
	// Minutes of hour of day. Must be from 0 to 59.
	Minutes uint8 `json:"minutes,omitempty"`
	// Seconds of minutes of the time. Must normally be from 0 to 59.
	// An API may allow the value 60 if it allows leap-seconds.
	Seconds uint8 `json:"seconds,omitempty"`
	// Fractions of seconds in nanoseconds. Must be from 0 to 999,999,999.
	Nanos uint64 `json:"nanos,omitempty"`
}

// FinalResponse is the final response when the user input is not expected.
type FinalResponse struct {
	RichResponse RichResponse `json:"richResponse,omitempty"` // Rich response when user is not required to provide an input.
}

// ExpectedInput is the input the app expects
type ExpectedInput struct {
	// The customized prompt used to ask user for input.
	InputPrompt InputPrompt `json:"inputPrompt,omitempty"`
	// List of intents that can be used to fulfill this input.
	// To have the Google Assistant just return the raw user input, the app should ask for the actions.intent.TEXT intent.
	PossibleIntents []ExpectedIntent `json:"possibleIntents,omitempty"`
	// List of phrases the app wants Google to use for speech biasing. Up to 1000 phrases are allowed.
	SpeechBiasingHints []string `json:"speechBiasingHints,omitempty"`
}

// InputPrompt is used for assistant to guide user to provide an input for the app's question.
type InputPrompt struct {
	RichInitialPrompt RichResponse     `json:"richInitialPrompt,omitempty"` // Prompt payload.
	NoInputPrompts    []SimpleResponse `json:"noInputPrompts,omitempty"`    // Prompt used to ask user when there is no input from user.
}

// SimpleResponse containing speech or text to show the user.
type SimpleResponse struct {
	// Plain text of the speech output, e.g., "where do you want to go?" Mutually exclusive with ssml.
	TextToSpeech string `json:"textToSpeech,omitempty"`
	// Structured spoken response to the user in the SSML format, e.g.
	// <speak> Say animal name after the sound. <audio src = 'https://www.pullstring.com/moo.mps' />,
	// what’s the animal? </speak>. Mutually exclusive with textToSpeech.
	SSML string `json:"ssml,omitempty"`
	// Optional text to display in the chat bubble. If not given, a display rendering of the textToSpeech or ssml above will be used.
	// Limited to 640 chars.
	DisplayText string `json:"displayText,omitempty"`
}

// RichResponse that can include audio, text, cards, suggestions and structured data.
type RichResponse struct {
	// A list of UI elements which compose the response
	// https://developers.google.com/actions/reference/rest/Shared.Types/AppResponse
	Items []Item `json:"items,omitempty"`
	// A list of suggested replies. These will always appear at the end of the response. If used in a FinalResponse, they will be ignored.
	Suggestions []Suggestion `json:"suggestions,omitempty"`
	// An additional suggestion chip that can link out to the associated app or site.
	LinkOutSuggestion LinkOutSuggestion `json:"linkOutSuggestion,omitempty"`
}

// Suggestion chip that the user can tap to quickly post a reply to the conversation.
type Suggestion struct {
	// The text shown the in the suggestion chip. When tapped,
	// this text will be posted back to the conversation verbatim as if the user had typed it.
	// Each title must be unique among the set of suggestion chips.
	// Max 25 chars. Required.
	Title string `json:"title,omitempty"`
}

// LinkOutSuggestion creates a suggestion chip that allows the user to jump out to the App or Website associated with this agent.
type LinkOutSuggestion struct {
	// The name of the app or site this chip is linking to.
	// The chip will be rendered with the title "Open ".
	// Max 20 chars. Required.
	DestinationName string `json:"destinationName,omitempty"`
	// The URL of the App or Site to open when the user taps the suggestion chip.
	// Ownership of this URL must be validated in the Actions on Google developer console,
	// or the suggestion will not be shown to the user.
	URL string `json:"url,omitempty"`
}

// ExpectedIntent the app is asking the assistant to provide.
type ExpectedIntent struct {
	// The built-in intent name, e.g. actions.intent.TEXT, or intents defined in the action package.
	// If the intent specified is not a built-in intent, it is only used for speech biasing and
	// the input provided by the Google Assistant will be the actions.intent.TEXT intent.
	Intent string `json:"intent,omitempty"`
	// Additional configuration data required by a built-in intent. See InputValue* const for possible values
	InputValueData ExtensionType `json:"inputValueData,omitempty"`
	// Optionally, a parameter of the intent that is being requested.
	// Only valid for requested intents. Used for speech biasing.
	ParameterName string `json:"parameterName,omitempty"`
	// Optionally, associate a dynamic set of triggers with this expected intent. Used for speech biasing.
	Trigger Trigger `json:"trigger,omitempty"`
}

const (
	InputValueOption          = "actions.intent.OPTION"                         // google.actions.v2.OptionValueSpec,
	InputValueConfirmation    = "actions.intent.CONFIRMATION"                   // google.actions.v2.ConfirmationValueSpec,
	InputValueTransCheck      = "actions.intent.TRANSACTION_REQUIREMENTS_CHECK" // google.actions.v2.TransactionRequirementsCheckSpec,
	InputValueDeliveryAddress = "actions.intent.DELIVERY_ADDRESS"               // google.actions.v2.DeliveryAddressValueSpec,
	InputValueTransDecision   = "actions.intent.TRANSACTION_DECISION"           // google.actions.v2.TransactionDecisionValueSpec
)

// Trigger specyfication
type Trigger struct {
	// List of patterns used to identify the specified intent.
	// Query patterns must only refer to parameters declared in the parameters field.
	QueryPatterns []string `json:"queryPatterns,omitempty"`
}

// Item of the response.
type Item struct {
	// Union field item can be only one of the following:

	// Voice and text-only response.
	SimpleResponse SimpleResponse `json:"simpleResponse,omitempty"`
	// A basic card.
	BasicCard BasicCard `json:"basicCard,omitempty"`
	// Structured payload to be processed by Google.
	StructuredResponse StructuredResponse `json:"structuredResponse,omitempty"`

	// End of list of possible types for union field item.
}

// BasicCard is a basic card for displaying some information, e.g. an image and/or text.
type BasicCard struct {
	// Overall title of the card. Optional.
	Title string `json:"title,omitempty"`
	// Subtitle is optional
	Subtitle string `json:"subtitle,omitempty"`
	// Body text of the card. Supports a limited set of markdown syntax for formatting. Required, unless image is present.
	FormattedText string `json:"formattedText,omitempty"`
	// A hero image for the card. The height is fixed to 192dp. Optional.
	Image Image `json:"image,omitempty"`
	// Buttons. Currently at most 1 button is supported. Optional.
	Buttons []Button `json:"buttons,omitempty"`
}

// Image displayed in the card.
type Image struct {
	// The source url of the image. Images can be JPG, PNG and GIF (animated and non-animated).
	// For example,https://www.agentx.com/logo.png. Required.
	URL string `json:"url"`
	// A text description of the image to be used for accessibility, e.g. screen readers. Required.
	AccessibilityText string `json:"accessibilityText"`
	// The height of the image in pixels. Optional.
	Height int64 `json:"height,omitempty"`
	// The width of the image in pixels. Optional.
	Width int64 `json:"width,omitempty"`
}

// Button object that usually appears at the bottom of a card.
type Button struct {
	Title         string        `json:"title"`         // Title of the button. Required.
	OpenURLAction OpenURLAction `json:"openUrlAction"` // Action to take when a user taps on the button. Required.
}

// OpenURLAction opens the given url.
type OpenURLAction struct {
	URL string `json:"url"` // http or https scheme url. Required.
}

// StructuredResponse is the response defined for app to respond with structured data.
type StructuredResponse struct {
	OrderUpdate OrderUpdate `json:"orderUpdate,omitempty"` // App provides an order update (e.g. Receipt) after receiving the order.
}

// OrderUpdate is update to an order.
type OrderUpdate struct {
	// Id of the order is the Google-issued id.
	GoogleOrderID string `json:"googleOrderId"`
	// Required. The canonical order id referencing this order.
	// If integrators don't generate the canonical order id in their system,
	// they can simply copy over googleOrderId included in order.
	ActionOrderID string `json:"actionOrderId"`
	// The new state of the order.
	OrderState OrderState `json:"orderState,omitempty"`
	// Updated applicable management actions for the order, e.g. manage, modify, contact support.
	OrderManagementActions []Action `json:"orderManagementActions,omitempty"`
	// Receipt for order.
	Receipt Receipt `json:"receipt,omitempty"`
	// When the order was updated from the app's perspective.
	UpdateTime time.Time `json:"updateTime,omitempty"`
	// New total price of the order
	TotalPrice Price `json:"totalPrice,omitempty"`
	// Map of line item-level changes, keyed by item id. Optional.
	// An object containing a list of "key": value pairs. Example: { "name": "wrench", "mass": "1.3kg", "count": "3" }.
	LineItemUpdates map[string]LineItemUpdate `json:"lineItemUpdates,omitempty"`
	// If specified, displays a notification to the user with the specified title and text.
	// Specifying a notification is a suggestion to notify and is not guaranteed to result in a notification.
	UserNotification UserNotification `json:"userNotification,omitempty"`
	// Extra data based on a custom order state or in addition to info of a standard state.
	InfoExtension ExtensionType `json:"infoExtension,omitempty"`

	// Union field info can be only one of the following

	RejectionInfo    RejectionInfo    `json:"rejectionInfo,omitempty"`    // Information about rejection state.
	CancellationInfo CancellationInfo `json:"cancellationInfo,omitempty"` // Information about cancellation state.
	InTransitInfo    InTransitInfo    `json:"inTransitInfo,omitempty"`    // Information about in transit state.
	FulfillmentInfo  FulfillmentInfo  `json:"fulfillmentInfo,omitempty"`  // Information about fulfillment state.
	ReturnInfo       ReturnInfo       `json:"returnInfo,omitempty"`       // Information about returned state.

	// End of list of possible types for union field info.
}

// OrderState is current order state.
type OrderState struct {
	// State is required and it can be one of the following values:
	//  CREATED: Order was created at integrator's system.
	//  REJECTED: Order was rejected by integrator.
	//  CONFIRMED: Order was confirmed by the integrator and is active.
	//  CANCELLED: User cancelled the order.
	//  IN_TRANSIT: Order is being delivered.
	//  RETURNED: User did a return.
	//  FULFILLED: User received what was ordered.
	//  CHANGE_REQUESTED: User has requested a change to the order, and the integrator is processing this change.
	//                    The order should be moved to another state after the request is handled.
	State string `json:"state,omitempty"`
	// The user-visible string for the state. Required.
	Label string `json:"label,omitempty"`
}

// Action is a follow-up action associated with the order update.
type Action struct {
	Type   ActionType `json:"type,omitempty"`   // Type of action.
	Button Button     `json:"button,omitempty"` // Button label and link.
}

// ActionType is a follow-up action
type ActionType string

const (
	ActionUnknown         ActionType = "UNKNOWN"          // Unknown action.
	ActionViewDetails     ActionType = "VIEW_DETAILS"     // View order details action on merchant side.
	ActionModified        ActionType = "MODIFY"           // Modify order action.
	ActionCancel          ActionType = "CANCEL"           // Cancel order action.
	ActionReturn          ActionType = "RETURN"           // Return order action.
	ActionExchange        ActionType = "EXCHANGE"         // Exchange order action.
	ActionEmail           ActionType = "EMAIL"            // Email action.
	ActionCall            ActionType = "CALL"             // Call action.
	ActionReorder         ActionType = "REORDER"          // Reorder action.
	ActionReview          ActionType = "REVIEW"           // Review order action.
	ActionCustomerService ActionType = "CUSTOMER_SERVICE" // Contact customer service.
)

// Receipt when state is CONFIRMED or any other state (e.g. IN_TRANSIT, FULFILLED) inclusive of CONFIRMED state.
type Receipt struct {
	// Confirmed order id when order has been received by the integrator. This is the canonical order id used in integrator's system referencing the order and may subsequently be used to identify the order as actionOrderId.
	// It is deprecated.
	ConfirmedActionOrderId string `json:"confirmedActionOrderId,omitempty"`
	// Optional. The user facing id referencing to current order, which will show up in the receipt card if present.
	// https://developers.google.com/actions/reference/rest/Shared.Types/AppResponse
	UserVisibleOrderId string `json:"userVisibleOrderId,omitempty"`
}

// Price in order
type Price struct {
	Type   PriceType `json:"type"`   // Type of price. Required.
	Amount Money     `json:"amount"` // Monetary amount. Required.
}

// PriceType is a type of the order price. See const for possible values
type PriceType string

const (
	PriceUnknown  PriceType = "UNKNOWN"  //Unknown price.
	PriceEstimate PriceType = "ESTIMATE" //Estimated price.
	PriceActual   PriceType = "ACTUAL"   //Actual price.
)

// Money represents an amount of money with its currency type.
type Money struct {
	// The 3-letter currency code defined in ISO 4217.
	CurrencyCode string `json:"currencyCode,omitempty"`
	// The whole units of the amount. For example if currencyCode is "USD", then 1 unit is one US dollar.
	// TODO(luke): Figure out the type, see the doc, probably it showul be int64
	Units string `json:"units,omitempty"`
	// Number of nano (10^-9) units of the amount.
	// https://developers.google.com/actions/reference/rest/Shared.Types/Price#Money
	Nanos int64 `json:"nanos,omitempty"`
}

// LineItemUpdate specifies item-level changes
type LineItemUpdate struct {
	// New line item-level state.
	OrderState OrderState `json:"orderState,omitempty"`
	// New price for the line item.
	Price Price `json:"price,omitempty"`
	// Reason for the change. Required for price changes.
	Reason string `json:"reason,omitempty"`
	// Update to the line item extension. Type must match the item's existing extension type.
	Extension ExtensionType `json:"extension,omitempty"`
}

// ExtensionType describes an object containing fields of an arbitrary type.
// An additional field "@type" contains a URI identifying the type.
// Example: { "id": 1234, "@type": "types.example.com/standard/id" }.
type ExtensionType map[string]interface{}

// UserNotification to display with a request.
type UserNotification struct {
	Title string `json:"title,omitempty"` // The title for the notification.
	Text  string `json:"text,omitempty"`  // The content of the notification.
}

// RejectionInfo when state is REJECTED.
// This message can be populated in the initial order update in conversation or through subsequent async order update.
type RejectionInfo struct {
	Type   ReasonType `json:"type,omitempty"`   // Rejection type
	Reason string     `json:"reason,omitempty"` // Reason for the error.
}

// ReasonType is a type of rejection, see Reason* for possible values
type ReasonType string

const (
	ReasonUnknown         ReasonType = "UNKNOWN"          //Unknown reason.
	ReasonPaymentDeclined ReasonType = "PAYMENT_DECLINED" //Payment declined.
	ReasonIneligible      ReasonType = "INELIGIBLE"       //User ineligible to place order (blacklisted).
)

// CancellationInfo when state is CANCELLED.
type CancellationInfo struct {
	Reason string `json:"reason,omitempty"` // Reason for cancellation.
}

// InTransitInfo when state is IN_TRANSIT.
type InTransitInfo struct {
	UpdateTime time.Time `json:"updateTime,omitempty"` // Last updated time for in transit.
}

// FulfillmentInfo when state is FULFILLED.
type FulfillmentInfo struct {
	DeliveryTime time.Time `json:"deliveryTime,omitempty"` // When the order will be fulfilled.
}

// ReturnInfo when state is REJECTED.
type ReturnInfo struct {
	Reason string `json:"reason,omitempty"` // Reason for return.
}
